package spitter.data;

import spitter.models.Spittle;

import java.util.List;

/**
 * Created by alejandro on 2/19/17.
 */
public interface SpittleRepository {
    List<Spittle> findSpittles(long max, int count);
}
