package spitter.web;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.HashMap;
import java.util.Map;

import static org.springframework.web.bind.annotation.RequestMethod.GET;

/**
 * Created by alejandro on 2/19/17.
 */
@Controller
public class CustomController {
    @RequestMapping(value = "/custom", method = GET)
    public String customView(@RequestParam(value="header") String header,
                           @RequestParam(value="body") String body){
        Map map = new HashMap<>();
        map.put("header", header);
        map.put("body", body);
        return "customView";

    }
}
